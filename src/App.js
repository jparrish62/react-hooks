import React from 'react';
import './App.css';
import DataFetchingThree from './components/DataFetchingThree'

function App() {
  return (
    <div className="App">
			<DataFetchingThree />
    </div>
  );
}

export default App;
