import React, {useState, useEffect} from 'react'

const HookMouse = () => {
	const [x, setX] = useState(0)
	const [y, setY] = useState(0)
	const logMousePosition = e => {
		console('Mouse event')
		setX(e.clientX)
		setY(e.clientY)
	} 
  /* Empty array calls useEffect only once*/
  /* add return to remove listeners*/

	useEffect(() => {
		console.log('useEffect called')
		window.addEventListener('mousePosition ', logMousePosition)

		return () => {
			console.log('Component unmounting code')
			window.removeEventListener('mousemove', logMousePosition)
		}
	}, [])

	return (
		<div>
      Hooks X - {x}  Y - {y}
		</div>
	)
}

export default HookMouse
