import React, {useState} from 'react'

const HookCounterTwo = () => {
	const initialCount = 0
	const [count, setCount] = useState(initialCount)
	const incrementFive = () => {
		for(let i = 0; i < 5; i++) {
			setCount(prevCount => prevCount + 1)
		}
	}
	return (
		<div>
			Count: {count}
			<button onClick={() => setCount(initialCount)}>Reset</button>
			<button onClick={() => setCount(count + 1)}>Increment</button>
			<button onClick={() => setCount(count - 1)}>Decrement</button>
				{/*When you have to set state to prvious state value pass, in a function 
				to the state getter*/}
			<button onClick={incrementFive}>Increment 5</button>
		</div>
	)
}
export default HookCounterTwo
