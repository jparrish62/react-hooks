import React, {useState, useEffect} from 'react'

const HookCounterFive = () => {
	const [count, setCount] = useState(0)
	const [name, setName] = useState("")

	/* executes after every render and every component update useEffect*/
	/* [count] conditionally runs useEffect, [count] represent what useEffect watches
		* only updates if count changes*/

	useEffect(() => {
		document.title = `You clicked ${count} times`
	}, [count])
	return (
		<div>
			<input type='text' value={name} onChange={e => setName(e.target.value)}/>
			<button onClicked={() => setCount(count + 1)}>Click {count} times</button>
		</div>
	)
}

export default HookCounterFive
